var getRawBody = require('raw-body')

// All these helpers and flags are public.
var context = module.exports = {
  // Default body limit in bytes.
  // Overwrite this yourself.
  limit: 1 * 1024 * 1024,
  // Default urlencoded parser.
  // Overwrite this yourself.
  urlencodedParser: require('querystring').parse
}

/*

  Parse the JSON body asynchronously.

    var body = yield this.json

  @api public

*/

context.json =
context.jsonBody =
context.JSONBody =
context.parseJSON =
context.parseJson = function* () {
  if (!this.is('json'))
    return

  // For those terrible clients that send
  // a content-type header with content-length: 0
  if (this.length === 0)
    return {}

  var buf = (yield this.rawBody).toString('utf8').trim()
  if (!buf.length)
    this.error(400, 'invalid json, empty body')

  var first = buf[0]
  if ('{' !== first && '[' !== first)
    this.error(400, 'invalid json')

  try {
    return JSON.parse(buf, this.JSONReviver)
  } catch (err) {
    err.status = 400
    throw err
  }
}

/*

  Parse the url encoded body asynchronously.

    var body = yield this.form

  @api public

*/

context.form =
context.formBody =
context.urlEncodedBody =
context.URLEncodedBody =
context.parseUrlencoded =
context.parseURLEncoded = function* () {
  if (!this.is('application/x-www-form-urlencoded'))
    return

  if (this.length === 0)
    return {}

  var buf = (yield this.rawBody).toString('utf8').trim()
  if (!buf.length)
    return {}

  try {
    return this.urlencodedParser(buf)
  } catch (err) {
    err.status = 400
    throw err
  }
}

/*

  Returns the entire request body as a single Buffer.

    var buf = yield this.rawBody

  @api public

*/

context.rawBody = function (done) {
  var stream = this.decodeRequest
    ? this.decodeRequest()
    : this.req

  getRawBody(stream, {
    expected: stream.headers && stream.headers['content-length'],
    limit: this.limit
  }, done)
}
